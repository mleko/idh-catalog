To start application run
```
docker-compose up
```

To init database, dependencies etc run
```
./bin/init
```

User / Pass
```
admin / admin
```
