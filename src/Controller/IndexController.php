<?php
declare(strict_types=1);

namespace App\Controller;


use App\Entity\Product;
use App\Product\Command\AddNewProduct;
use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;
use League\Tactician\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    public function index(Request $request) {
        /** @var EntityManager $doctrine */
        $doctrine = $this->get("doctrine.orm.entity_manager");
        $query = $doctrine->createQuery("SELECT p FROM " . Product::class . " p");

        /** @var Paginator $paginator */
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10,/*limit per page*/
            ["defaultSortFieldName" => "p.creationTime", "defaultSortDirection" => "desc"]
        );

        return $this->render("products.html.twig", [
            "pagination" => $pagination,
            "currency" => $this->getCurrencyCode($request)
        ]);
    }

    public function form(Request $request, CommandBus $commandBus) {
        $product = new Product();

        $form = $this->createFormBuilder($product)
            ->add("name", TextType::class)
            ->add("description", TextType::class)
            ->add("price", MoneyType::class, ["currency" => $this->getCurrencyCode($request)])
            ->add("save", SubmitType::class, ["label" => "Submit"])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();

            $commandBus->handle(new AddNewProduct($product));
            $this->addFlash("success", "Product added");
            return $this->redirectToRoute("product.add");
        }

        return $this->render("form.html.twig", ["form" => $form->createView()]);
    }

    /**
     * @param Request $request
     * @return bool|string
     */
    public function getCurrencyCode(Request $request) {
        return \NumberFormatter::create($request->getDefaultLocale(), \NumberFormatter::CURRENCY)->getTextAttribute(\NumberFormatter::CURRENCY_CODE);
    }
}
