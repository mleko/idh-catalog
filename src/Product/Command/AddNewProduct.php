<?php
declare(strict_types=1);

namespace App\Product\Command;


use App\Entity\Product;

class AddNewProduct
{
    /** @var Product */
    private $product;

    /**
     * AddNewProduct constructor.
     * @param Product $product
     */
    public function __construct(Product $product) {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product {
        return $this->product;
    }

}
