<?php
declare(strict_types=1);

namespace App\Product\Listener;


use App\Product\Event\NewProductAdded;

class NotifyAboutNewProduct
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * NotifyAboutNewProduct constructor.
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer) {
        $this->mailer = $mailer;
    }

    public function handle(NewProductAdded $event) {
        $message = new \Swift_Message("New product added", "Added new product: " . $event->getProduct()->getName());
        $message->setFrom("fake@example.com");
        $message->setTo("fake@example.com");
        $this->mailer->send($message);
    }
}
