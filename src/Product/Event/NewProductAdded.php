<?php
declare(strict_types=1);

namespace App\Product\Event;


use App\Entity\Product;

class NewProductAdded
{
    /**
     * @var Product
     */
    private $product;

    /**
     * NewProductAdded constructor.
     * @param Product $product
     */
    public function __construct(Product $product) {
        $this->product = $product;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product {
        return $this->product;
    }

}
