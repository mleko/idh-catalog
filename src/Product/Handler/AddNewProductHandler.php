<?php
declare(strict_types=1);

namespace App\Product\Handler;


use App\Product\Command\AddNewProduct;
use App\Product\Event\NewProductAdded;
use Doctrine\ORM\EntityManagerInterface;
use Mleko\Narrator\EventEmitter;

class AddNewProductHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EventEmitter
     */
    private $emitter;

    /**
     * AddNewProductHandler constructor.
     * @param EntityManagerInterface $entityManager
     * @param EventEmitter $emitter
     */
    public function __construct(EntityManagerInterface $entityManager, EventEmitter $emitter) {
        $this->entityManager = $entityManager;
        $this->emitter = $emitter;
    }

    public function handle(AddNewProduct $command) {
        $this->entityManager->persist($command->getProduct());

        $this->emitter->emit(new NewProductAdded($command->getProduct()));
    }
}
