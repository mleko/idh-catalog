<?php
declare(strict_types=1);

namespace App\Entity;


use Ramsey\Uuid\Uuid;

class Product
{
    /** @var Uuid */
    private $id;
    /** @var string */
    private $name = "";
    /** @var string */
    private $description = "";
    /** @var int */
    private $price = 0;
    /** @var float */
    private $creationTime;

    /**
     * Product constructor.
     * @param Uuid|null $uuid
     * @throws \Exception
     */
    public function __construct(Uuid $uuid = null) {
        $this->id = $uuid ?: Uuid::uuid4();
        $this->creationTime = \microtime(true);
    }

    /**
     * @return Uuid
     */
    public function getId(): Uuid {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): string {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getPrice(): int {
        return $this->price;
    }

    /**
     * @param int $price
     */
    public function setPrice(int $price): void {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getCreationTime(): float {
        return $this->creationTime;
    }

}
