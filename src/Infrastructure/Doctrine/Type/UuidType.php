<?php
declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Type;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class UuidType extends Type
{

    /**
     * Gets the SQL declaration snippet for a field of this type.
     *
     * @param array $fieldDeclaration The field declaration.
     * @param \Doctrine\DBAL\Platforms\AbstractPlatform $platform The currently used database platform.
     *
     * @return string
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform) {
        $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }

    /**
     * Gets the name of this type.
     *
     * @return string
     */
    public function getName() {
        return "uuid";
    }

    /**
     * @inheritDoc
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        if (null === $value) {
            return null;
        }
        if ($value instanceof UuidInterface) {
            return $value->toString();
        }
        if(\is_string($value)){
            return Uuid::fromString($value)->toString();
        }
        throw new \RuntimeException("Invalid UUID value");
    }

    /**
     * @inheritDoc
     */
    public function convertToPHPValue($value, AbstractPlatform $platform) {
        if (null === $value || "" === $value) {
            return null;
        }
        if ($value instanceof UuidInterface) {
            return $value;
        }
        return Uuid::fromString($value);
    }


}
