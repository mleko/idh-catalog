<?php
declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Fixtures;


use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BaseFixtures extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager) {
        for ($i = 0; $i < 34; $i++) {
            $product = new Product();
            $product->setName("product-$i");
            $product->setDescription(\str_repeat("Lorem ipsum dolor sit amet, consectetur adipiscing elit. ", 3));
            $product->setPrice(1 + 33 * $i);
            $manager->persist($product);
        }

        $manager->flush();
    }
}
