<?php declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180904175120 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $table = $schema->createTable("products");
        $table->addColumn("id", "string");
        $table->addColumn("name", "string");
        $table->addColumn("description", "string");
        $table->addColumn("price", "integer");
        $table->setPrimaryKey(["id"]);

    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable("products");

    }
}
