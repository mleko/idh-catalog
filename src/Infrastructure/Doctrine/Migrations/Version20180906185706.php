<?php declare(strict_types=1);

namespace App\Infrastructure\Doctrine\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180906185706 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $table = $schema->getTable("products");
        $table->addColumn("creation_time", "float", ["default" => "0"]);

    }

    public function down(Schema $schema) : void
    {
        $schema->getTable("products")->dropColumn("creation_time");

    }
}
