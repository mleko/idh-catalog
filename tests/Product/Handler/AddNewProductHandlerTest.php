<?php
declare(strict_types=1);

namespace App\Tests\Product\Handler;

use App\Entity\Product;
use App\Product\Command\AddNewProduct;
use App\Product\Handler\AddNewProductHandler;
use Doctrine\ORM\EntityManagerInterface;
use Mleko\Narrator\EventEmitter;
use PHPUnit\Framework\TestCase;

class AddNewProductHandlerTest extends TestCase
{

    public function testHandle() {
        $em = $this->getMockForAbstractClass(EntityManagerInterface::class);
        $em->expects($this->once())->method("persist");

        $emitter = $this->getMockForAbstractClass(EventEmitter::class);
        $emitter->expects($this->once())->method("emit");
        (new AddNewProductHandler($em, $emitter))->handle(new AddNewProduct(new Product()));
    }
}
