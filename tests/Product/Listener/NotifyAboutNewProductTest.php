<?php
declare(strict_types=1);

namespace App\Tests\Product\Listener;

use App\Entity\Product;
use App\Product\Event\NewProductAdded;
use App\Product\Listener\NotifyAboutNewProduct;
use PHPUnit\Framework\TestCase;

class NotifyAboutNewProductTest extends TestCase
{

    public function testHandle() {
        $mailer = $this->createMock(\Swift_Mailer::class);
        $mailer->expects($this->once())->method("send");
        (new NotifyAboutNewProduct($mailer))->handle(new NewProductAdded(new Product()));
    }
}
